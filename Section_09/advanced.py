from random import randint

print("... Schere ...")
print("... Stein ...")
print("... Papier ...")

choice_player1 = str.upper(input("Spieler 1, bitte Auswahl eingeben: "))
player2 = randint(1, 3)
if player2 == 1:
    choice_player2 = "SCHERE"
elif player2 == 2:
    choice_player2 = "STEIN"
else:
    choice_player2 = "PAPIER"

print(f"Computer wählt {choice_player2}")

if choice_player1 == choice_player2:
    print("Unentschieden, kein Gewinner!")
else:
    if choice_player1 == "SCHERE":
        if choice_player2 == "STEIN":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "PAPIER":
            print("Spieler 1 gewinnt!")

    elif choice_player1 == "STEIN":
        if choice_player2 == "PAPIER":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "SCHERE":
            print("Spieler 1 gewinnt!")

    elif choice_player1 == "PAPIER":
        if choice_player2 == "SCHERE":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "STEIN":
            print("Spieler 1 gewinnt!")

    else:
        print("Uuups. Da ist etwas schief gelaufen!")