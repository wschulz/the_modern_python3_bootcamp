print("... Schere ...")
print("... Stein ...")
print("... Papier ...")

choice_player1 = input("Spieler 1, bitte Auswahl eingeben: ")
choice_player2 = input("Spieler 2, bitte Auswahl eingeben: ")

print("SHOOT")

if choice_player1 == choice_player2:
    print("Unentschieden, kein Gewinner!")
else:
    if choice_player1 == "Schere":
        if choice_player2 == "Stein":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "Papier":
            print("Spieler 1 gewinnt!")

    elif choice_player1 == "Stein":
        if choice_player2 == "Papier":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "Schere":
            print("Spieler 1 gewinnt!")

    elif choice_player1 == "Papier":
        if choice_player2 == "Schere":
            print("Spieler 2 gewinnt!")
        elif choice_player2 == "Stein":
            print("Spieler 1 gewinnt!")

    else:
        print("Uuups. Da ist etwas schief gelaufen!")