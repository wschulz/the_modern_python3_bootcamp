#   Symbol  Name
#   +       Addition
#   -       Subtraction
#   *       Multiplication
#   /       Division
#   **      Exponentiation
#   %       Modulo
#   //      Integer Division

print(1 + 3)
print(40-4.5)

# Division always give a float back
print(1/2)
print(1/3)

# Hierarchy

# 1. Parenteses
# 2. Exponents
# 3. Multiplication
# 4. Division
# 5. Addition
# 6. Subtraction

print(1 + 2 * 5 / 3)
print((1 + 2) * 5 / 3)

# Exponential
print(2 ** 3)
print(4 ** 8)

# Takes the square root
print(49 ** 0.5)

# Modulo gives us the remainder
print(10 % 3)
print(25 % 4)
print(13 % 2)

# Integer Division gives an integer back and rouds down
print(10//3)
print(6//7)
