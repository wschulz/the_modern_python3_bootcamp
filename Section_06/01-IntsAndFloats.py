# Integers
4
57
-10

# Floating Point Numbers (Floats)
6.1
1.3333
0.0


print(type(9))

print(type(9999.2))

# Give back an int
print(1 + 3)

# If any part of the expression is a float, the whole number will be a float
print(1 + 1.0)

print(20 + 0.77)