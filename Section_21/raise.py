def colorize(text, color):
    colors = ("cyan", "magenta", "yellow", "blue", "green")
    if type(text) is not str:
        raise TypeError("Text must be instance of str")
    if color not in colors:
        raise ValueError("Color must be: cyan, magenta, yellow, blue, green")
    print(f"Printed {text} in {color}")

colorize("hello", "cyan")
colorize(34, "red")