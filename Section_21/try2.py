# try:
# except:
# else:
# finally:

# try:
#     num = int(input("Please enter a number: "))
# except:
#     print("That's not a number!")
# else:
#     print("I will run when NO exeption is thrown")
# finally:
#     print("I will run, no matter what!")

while True:
    try:
        num = int(input("Please enter a number: "))
    except ValueError:
        print("That's not a number!")
    else:
        print("Good Job, you entered a number.")
        break
    finally:
        print("I will run, no matter what!")
print("Rest of Game Logic runs")