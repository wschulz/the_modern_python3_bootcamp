my_other_string = 'a hat'
my_str = "a cat"

name = 'Colt'
name2 = "Colt"

print(name)
print(name2)

# msg = "he said "hello there""  ### this will throw an error, no double quotes inside double quotes
msg = 'he said "hello there"'
msg2 = "I'm funny"

# ESCAPE CHARACTERS (ESCAPE SEQUENCES)
new_line = "hello\nworld"
print(new_line)

new_string = "This is a backslash: \\"
print(new_string)

new_sentence = "He said \"Ha ha\""
print(new_sentence)


# STRING CONCATENATION
str_one = "your"
str_two = "face"
str_three = "Punch in " + str_one + " " + str_two
print(str_three)

str_four = "ice"
str_four += " cream"
print(str_four)

# FORMATTING STRINGS
# F-Strings

x = 10
formatted = f"I've told you at least {x} times!"
print(formatted)

cat_name = "blue"
print(f"Nice try, but the quess of {cat_name} was incorrect!")

# Formating before Python 3.6 (not recommended anymore)
fruit = "banana"
ripeness = "unripe"
print("The {} is {}.".format(fruit,  ripeness))

# String index
name_hero = "Chuck"
print(name_hero[0])
print(name_hero[-1])
