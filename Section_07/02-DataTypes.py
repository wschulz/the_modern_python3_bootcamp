# The most commonly used datatypes:

# bool  True or False
# int   an integer (1, 2, 3)
# str   (string) a sequence of Unicode characters, ex. "Colt"
# list  an ordered sequence of values of other data types, ex. [1, 2, 3] or ["a", "b", "c"]
# dict  a collection of key:values, ex. {"first_name":"Colt", "last_name":"Steele"}

# Among others!

# Booleans
is_active = True
print(is_active)

game_over = False
print(game_over)

# game_over = false  ### This gives an error, because the keyword is false

# Strings
some_string = "8"
another_string = "hello I am a string"

# DYNAMIC TYPING, varaibles can change type.
awesomeness = True
print(awesomeness)

awesomeness = "a dog"
print(awesomeness)

awesomeness = None
print(awesomeness)

awesomeness = 22 / 7
print(awesomeness)

# Converting Data Types
decimal_num = 12.56345634534
integer = int(decimal_num)
print(integer)

my_list = [1, 2, 3]
my_list_as_string = str(my_list)
print(my_list_as_string)
