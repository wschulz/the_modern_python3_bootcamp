# A variable in Python is like a variable in math: it is a named symbol that holds a value.

# Variables are always assigned with the variable name on the left and
# the value on right of the equal sign.

# Variables must be assigned before they can be used.

x = 100
khaleesi_mother_of_dragons = 1

print(khaleesi_mother_of_dragons + x)

num_of_cats = 99
print(num_of_cats)

print(num_of_cats * 2)
print(num_of_cats)

# Variable assignment
# Variables can be assigned to other variables
# Variables can be reassigned at any time

python_is_awesome = 100
just_another_var = python_is_awesome
python_is_awesome = 1337

more, at, once = 5, 10, 15
print(more + at + once)

num_of_cats = num_of_cats - 1
print(num_of_cats)

friends = 0
print(friends)

friends = num_of_cats
print(friends)
