# With a for loop
numbers = [1, 2, 3, 4, 5]
doubled_numbers = []

for num in numbers:
    doubled_number = num * 2
    doubled_numbers.append(doubled_number)
    
print(doubled_numbers)


# With list comprehension
numbers = [1, 2, 3, 4, 5]

doubled_numbers = [num * 2 for num in numbers]
print(doubled_numbers)

name = 'colt'
print([char.upper() for char in name])

print([num*10 for num in range(1, 6)])

print([bool(val) for val in [0, [], '']])


numbers = [1, 2, 3, 4, 5]
string_list = [str(num) for num in numbers]
print(string_list)


colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
print([color.upper() for color in colors])

nums = [1, 2, 3]
print([str(num) for num in nums])
print([str(num) + ' Hello' for num in nums])


# List comprehension with conditional logic
# if-Statement
numbers = [1, 2, 3, 4, 5, 6]
evens = [num for num in numbers if num % 2 == 0]
odds = [num for num in numbers if num % 2 != 0]
print(evens)
print(odds)

with_vowels = "This is so much fun!"
print(''.join(char for char in with_vowels if char not in "aeiou"))

# else-Statement
print([num*2 if num % 2 == 0 else num/2 for num in numbers])
