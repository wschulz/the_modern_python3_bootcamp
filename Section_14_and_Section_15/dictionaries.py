# DICTIONARIES ARE NOT ORDERED

cat = {"name": "blue", "age": 3.5, "is_cute": True}
cat2 = dict(name = "kitty", age = 0.5)

instructor = {"name": "Colt", "owns_dog": True, "num_courses": 4, "favorite_language": "Python", "is_hilarious": False, 44: "my favorite number!"}
print(instructor["name"])
# print(instructor["city"]) #Key value Error


#Extend the dictionary
cat["color"] = "green"


# Loop through the values
for value in instructor.values():
    print(value)


# Loop through the keys
for key in instructor.keys():
    print(key)


# Loop through both, the keys and the values (items)
for key, value in instructor.items():
    print(f"Key: {key}, Value: {value}")


# Test if a key is inside a dictionary
print("name" in instructor)
print("awesome" in instructor)

if "phone" in instructor:
    print(instructor["phone"])
else:
    print("No phone number found!")


# Test if a value is inside a dictionary
print("Colt" in instructor.values())
print("Nope!" in instructor.values())


# Clearing of all the values in a dictionary (clear):
d = dict(a=1, b=2, c=3)
d.clear()
print(d)


# Make a copy of a dictionary (copy):
d = dict(a=1, b=2, c=3)
c = d.copy()
print(c)
print(c == d)
print(c is d)


# Create key-value pairs from comma separated values (fromkeys):
new_user = {}.fromkeys(["name", "score", "email", "profile bio"], "unknown")
print(new_user)


# Retrieves a key in an object and return None instead of a KeyError if the key does not exist (get):
d = dict(a=1, b=2, c=3)
print(d["a"])
print(d.get("a"))
print(d["b"])
print(d.get("b"))
# print(d["no_key"]) # KeyError
print(d.get("no_key")) # None


# Takes a single argument corresponding to a key and removes that key-value pair from the dictionary. Returns the value
# corresponding to the key that was removed (pop).
d = dict(a=1, b=2, c=3)
# d.pop() #TypeError: pop expected at least 1 arguments, got 0
d.pop("a") # 1
d # {"c": 3, "b": 2}
# d.pop("e") # KeyError


# Removes a random key in a dictionary (popitem):
d = dict(a=1, b=2, c=3, d=4, e=5)
d.popitem() # Removes a random item
# d.popitem("a") # TypeError: popitem() takes no arguments (1 given)


# Update keys and values in a dictionary with another set of key value pairs.
first = dict(a=1, b=2, c=3, d=4, e=5)
second = {}

second.update(first)
second # {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}

# let's overwrite an existing key
second["a"] = "AMAZING"
second # {'a': 'AMAZING', 'b': 2, 'c': 3, 'd': 4, 'e': 5}

# another update overwrites all changes
second.update(first)
second # {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
