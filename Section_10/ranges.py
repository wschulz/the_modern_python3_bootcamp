# The range type represents an immutable sequence of numbers and is commonly used for looping a specific number of times in for loops.

# range(7) Count starts at 0 and stps at 6
# range(1, 8) give the numbers from 1 to 7
# range(1, 10, 2) give odd numbers from 1 to 10
# range(7,0,-1) give integers from 7 to 1

r = range(10)
list(r)

nums = range(1,10,2)
list(nums)

for num in range(10, 20, 2):
    print(num)