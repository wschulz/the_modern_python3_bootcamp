# _name
# __name
# __name__


class Person:
    def __init__(self):
        self.name = "Tony"
        self._secret = "hi!"
        self.__msg = "I like turtles!"
        self.__lol = "HaHaHaHaH"

    # def doorman(self, guess):
    #     if guess == self._secret:
    #         let them in


p = Person()


print(p.name)
print(p._secret)
# print(p.__msg)   ---- Will not work -> Name mangling
# print(dir(p))    <--- Shows the new Name: It's _Person__msg
print(p._Person__msg)
