# Code from the exercise:


class BankAccount:
    def __init__(self, owner):
        self.owner = owner
        self.balance = 0.0

    def get_balance(self):
        return self.balance

    def deposit(self, amount):
        self.balance = self.balance + amount
        return f"{self.balance}"

    def withdraw(self, amount):
        self.balance = self.balance - amount
        return f"{self.balance}"


acct = BankAccount("Darcy")
print(acct.owner)
print(acct.get_balance())
print(acct.deposit(10))
print(acct.withdraw(3))
print(acct.get_balance())
