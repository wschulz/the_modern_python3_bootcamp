max(3, 67, 99) # 99
max('c', 'd', 'a') # 'd'
max('awesome') # 'w'
max({1:'a', 3:'c', 2:'b'}) # 3

nums = [40, 32, 6, 5, 10]
max(nums) # 40
min(nums) # 5

max("hello world") # 'w'
min("hello world") # ' '

max((3, 5, 23, 65)) # 65

names = ["Aria", "Samson", "Dora", "Tim", "Ollivander"]
min(names) # Arya (Alphabetically)
min(len(name) for name in names) # 3 (Shows the len)

max(names, key=lambda n: len(n)) # 'Ollivander' (Shows now the longest name)
min(names, key=lambda n: len(n)) # 'Tim'