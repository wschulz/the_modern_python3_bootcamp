# any
# Returns True if any element of the iterable is truthy. If the iterable is empty, return False.

any([0, 1, 2, 3]) # True
any([val for val in [1, 2, 3] if val > 2]) # True
any([val for val in [1, 2, 3] if val > 5]) # False

nums = [2, 6, 26, 18, 21]
any([num % 2 == 0 for num in nums]) # True