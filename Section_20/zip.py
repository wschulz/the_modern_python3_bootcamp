# zip

# Makes an iterator that aggregates elemets from each of the provided iterables.

# Return an iterator of tuples, here the i-th tuple contains the i-th element
# from each of the argument sequences or iterables.

# The iterator stops when the shortest input iterable is exhausted.

first_zip = zip([1, 2, 3], [4, 5, 6])
list(first_zip) # [(1, 4), (2, 5), (3, 6)]
dict(first_zip) # {1: 4, 2: 5, 3: 6}

nums1 = [1, 2, 3, 4, 5, 6]
nums2 = [21, 34, 23, 56, 23, 77]
words = ["hi", "you", "milk", ";)", "angry"] 
list(zip(words, nums1, nums2)) # [('hi', 1, 21), ('you', 2, 34), ('milk', 3, 23), (';)', 4, 56), ('angry', 5, 23)]

# unzip ;)
five_by_two = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)]

list(zip(*five_by_two))    # [(0, 1, 2, 3, 4), (1, 2, 3, 4, 5)]


# The weird part
midterms = [80, 91, 78]
finals = [98, 89, 53]
students = ['dan', 'ang', 'kate']

# Both of the solutions are working

# The more complex way to get the results
final_grades = {data[0]:max(data[1], data[2]) for data in zip(students, midterms, finals)}
print(final_grades)


# The more pythonic, and traditional way to get the results
scores = dict(zip(students, map(lambda pair: max(pair), zip(midterms, finals))))
print(scores)

# And here the same for an average
avg = dict(zip(students, map(lambda pair: ((pair[0] + pair[1]) / 2), zip(midterms, finals))))
print(avg)