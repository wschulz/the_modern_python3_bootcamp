nums = [1, 2, 3, 4]

evens = list(filter(lambda x: x %2 == 0, nums))

print(evens) # [2, 4]


names = ["austin", "penny", "anthony", "angel", "billy"]
a_names = filter(lambda n: n[0] == "a", names)
print(list(a_names)) 


users = [
    {"username": "samuel", "tweets": ["I love cakes", "I love beer"]},
    {"username": "katie", "tweets": ["I love my cat"]},
    {"username": "jeff", "tweets": []},
    {"username": "bob123", "tweets": []},
    {"username": "dogg_luvr", "tweets": ["Dogs are the best"]},
    {"username": "guitar_gal", "tweets": []}
]

inactive_users = filter(lambda u: not u["tweets"], users)
print(list(inactive_users))