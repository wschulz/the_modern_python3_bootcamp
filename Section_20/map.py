# map is a common use-case for lambdas

nums = [1, 2, 3, 4]

doubles = list(map(lambda x: x*2, nums))
print(doubles) # [2,4,6,8]

names = [
    {'first': 'Rusty', 'last': 'Steele'},
    {'first': 'Colt', 'last': 'Steele'},
    {'first': 'Blue', 'last': 'Steele'}
]

first_names = list(map(lambda x: x['first'], names))
print(first_names) # ['Rusty', 'Colt', 'Blue']


# you dont have to use lambdas

def double(x):
    return x*2

doubles = list(map(double, nums))
print(doubles)