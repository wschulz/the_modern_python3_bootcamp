import tkinter as tk

root = tk.Tk()
frame = tk.Frame(root)
frame.pack()


def say_hi():
    print("Hello!")

# button = tk.Button(frame, text="Click me", fg="red", command=say_hi) # with a function
button = tk.Button(frame, text="Click me", fg="red", command=lambda: print("Hello")) # Same with a lambda, without make a function

button.pack(side=tk.LEFT)
root.mainloop()