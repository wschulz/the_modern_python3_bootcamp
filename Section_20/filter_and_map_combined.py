names = ['Lassie', 'Colt', 'Rusty']

print(list(map(lambda name: f"Your instructor is {name}",
    filter(lambda value: len(value) < 6, names))))



users = [
    {"username": "samuel", "tweets": ["I love cakes", "I love beer"]},
    {"username": "katie", "tweets": ["I love my cat"]},
    {"username": "jeff", "tweets": []},
    {"username": "bob123", "tweets": []},
    {"username": "dogg_luvr", "tweets": ["Dogs are the best"]},
    {"username": "guitar_gal", "tweets": []}
]

print(list(map(lambda user: user["username"].upper(), filter(lambda u: not u["tweets"], users))))