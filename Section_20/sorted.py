# sorted
# returns a new sorted list from the items in iterable

numbers = [6, 1, 8, 2]

sorted(numbers) # [1, 2, 6, 8]
print(numbers) # [6, 1, 8, 2]

sorted(numbers, reverse=True) # [8, 6, 2, 1]
print(numbers) # [6, 1, 8, 2]


# On a tuple
more_numbers = (2, 1, 45, 23, 99)
sorted(more_numbers) # [1, 2, 23, 45, 99]


#####

users = [
    {"username": "samuel", "tweets": ["I love cakes", "I love beer"]},
    {"username": "katie", "tweets": ["I love my cat"]},
    {"username": "jeff", "tweets": []},
    {"username": "bob123", "tweets": []},
    {"username": "dogg_luvr", "tweets": ["Dogs are the best"]},
    {"username": "guitar_gal", "tweets": []}
]

sorted(users, key=lambda user: user['username'])
sorted(users, key=lambda user: user[len('tweets')], reverse = True)
