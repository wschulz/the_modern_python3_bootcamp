# all
# Return True if all elements of the iterable are truthy (or if the iterable is empty)

all([0, 1, 2, 3]) # False
all([char for char in "eio" if char in "aeiou"]) # True
all([num for num in [4,2,10,6,8] if num % 2 == 0]) # True

people = ["Charlie", "Casey", "Cody", "Carly", "Cristina"]
all([name[0] == "C" for name in people]) # True

nums = [2, 60, 26, 18]
all([num % 2 == 0 for num in nums]) # True

nums = [2, 60, 26, 18, 21]
all([num % 2 == 0 for num in nums]) # False