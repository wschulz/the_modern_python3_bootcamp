# Contitional Statements

# if some condition is True:
#   do something
# elif some other condition is True:
#   do something
# else:
#   do something

name = "Jack Sparrow"

if name == "Arya Stark":
    print("Valar Morghulis")
elif name == "Jon Snow":
    print("You know nothing")
else:
    print("Carry on")


# Multiple elif's are possible

color = input("What is your favorite color? ")
if color == "purple":
    print("excellent choice!")
elif color == "teal":
    print("not bad!")
elif color == "seafoam":
    print("mediocre")
elif color == "pure darkness":
    print("I like how you think!")
else:
    print("YOU MONSTER!")