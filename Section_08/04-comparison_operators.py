#   a = 1   b = 1
#
#   Operator    What it does                                            Example
#   ==          Truthy if a has the same value as b                     a == b      #True
#   !=          Truthy if a does NOT have the same value as b           a != b      #False
#
#   >           Truthy if a is greater than b                           a > b       #False
#   <           Truthy if a is less than b                              a < b       #False
#
#   >=          Truthy if a is greater or equal to b                    a >= b      #True
#   <=          Truthy if a is less than or equal to b                  a <= b      #True    


#   is vs. ==

#   a = 1
#   a == 1  # True
#   a is 1  # True

#   a = [1, 2, 3]
#   b = [1, 2, 3]
#   a == b  # True
#   a is b  # False

#   c = b
#   b is c  # True
