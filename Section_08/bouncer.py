# ask for age
# 18 - 21 wristband
# 21+ drink, normal entry
# under 18, too young

age = input("How old are you? ")
if age:
    age = int(age)
    if age >=  21:
        print("You can enter and drink.")
    elif age >= 18:
        print("You can enter, but need a wristband.")
    else:
        print("Sorry! You are too young to enter. :(")
else:
    print("Please enter your age!")