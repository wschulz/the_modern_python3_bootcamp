#   Operator    What it does                                            Example
#
#   and         Truthy if both a AND b are true (logical conjunction)   if a and b:
#                                                                           print(c)
#
#   or          Truthy if either a OR b are true (logical disjunction)  if am_tired or is_bedtime:
#                                                                           print("go to sleep")
#
#   not         Truthy if the opposite of a is true (logical neogation) if not is_weekend:
#                                                                           print("go to work")

# AND
age = 6

if age > 2 and age < 8:
    print("Discount entry")

# OR
city = input("Where do you live? ")

if city == "los angeles" or city == "san francisco":
    print("You live in California")
else:
    print("You live somewhere else")

# NOT
age = 21
# 2-8   2 dollar ticket
# 65    5 dollar ticket
# other people 10 dollars

if not ((age >= 2 and age <= 8) or age >= 65):
    print("You pay 10 Dollars.")
elif age >= 65:
    print("You pay 5 Dollars.")
else:
    print("You pay 2 Dollars.")
