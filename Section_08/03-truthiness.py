# All conditional checks resolve to True or False

x = 1

x is 1 # True
x is 0 # False

# Naturally False
# Empty Objects, Empty Strings, None, Zero

animal = input("Enter your favorite animal ")
if animal:
    print(animal + " is my favorite too")