colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"]

# Start Point
print(colors[2:])
print(colors[-2:])

# End Point
print(colors[:4])
print(colors[1:3])
print(colors[1:-1])

# Step
print(colors[1::2])
print(colors[::2])

# Negative Step
print(colors[1::-1])
print(colors[:1:-1])

sentence = "This is fun!"
print(sentence[::-1])

print(colors[5][::-1])