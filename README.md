# Kursmaterialien zum Kurs 'The modern Python3 Bootcamp' von Colt Steele auf Udemy

## Sectionen 1-5:
Einleitung, Anmerkung zum Kurs

## Section 06:
Numbers and Math (schnell überflogen weil schon Erfahrungen aus anderen Sprachen)

## Section 07:
Variables and Data Types (schnell überflogen weil schon Erfahrungen aus anderen Sprachen)

## Section 08:
Boolean and Conditional Logic (schnell überflogen weil schon Erfahrungen aus anderen Sprachen)

## Section 09:
Rock, Paper, Scissors (Schere, Stein, Papier)

## Section 10:
Looping in Python

## Section 11:
Guessing Game Mini Project (as Exercise) and improving Rock, Paper, Scissors

## Section 12:
Lists

## Section 13:
List Comprehension

## Section 14:
Dictionaries

## Section 15:
Dictionary Exercises

## Section 16:
Tuples and Sets

## Section 17:
Functions Pt. 1

## Section 18:
About 15 Function Exercises

## Section 19:
Functions Pt. 2

## Section 20:
- Lambdas
- Built-In Functions

## Section 21:
Debugging and Error Handling

## Section 22:
Modules

## Section 23:
Making HTTP Requests with Python

## Section 24:
Object Oriented Programming

## Section 25:
Deck of Cards Exercise

## Section 26:
Object Oriented Programming - Part 2

## Section 27:
Iterators and Generators