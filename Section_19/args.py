def sum_all_nums(*args):
    total = 0
    for num in args:
        total += num
    return total

print(sum_all_nums(2,4,5))
print(sum_all_nums(13,2,34,1,23,4,56,54,12))


def ensure_correct_info(*args):
    if "Hugo" in args and "the Boss" in args:
        return "Welcome back Hugo!"
    return "Not sure who you are..."

print(ensure_correct_info())
print(ensure_correct_info(1, True, True, "the Boss", 13, "Hugo"))
