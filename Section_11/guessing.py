import random
play = "y"

while play == "y":
    random_number = random.randint(1,10)
    user_input = int(input("Guess a number between 1 and 10: "))
    while user_input != random_number:
        if user_input < random_number:
            user_input = int(input(f"{user_input} is too low, please chosse a higher number: "))
        else:
            user_input = int(input(f"{user_input} is too high, please chosse a lower number: "))
    print("Nice. You got it!")
    play = input("Do you want to play again? Type y or n! ")
print("Thank you. See you soon.")
