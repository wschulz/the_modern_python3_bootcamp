from random import randint

player_score = 0
computer_score = 0

winning_score = 2

while player_score < winning_score and computer_score < winning_score:
    print("... Schere ...")
    print("... Stein ...")
    print("... Papier ...")

    choice_player1 = str.upper(input("Spieler 1, bitte Auswahl eingeben: "))
    if choice_player1 == "QUIT" or choice_player1 == "Q":
        print("Spiel verlassen.")
        break

    player2 = randint(1, 3)
    if player2 == 1:
        choice_player2 = "SCHERE"
    elif player2 == 2:
        choice_player2 = "STEIN"
    else:
        choice_player2 = "PAPIER"

    print(f"Computer wählt {choice_player2}")

    if choice_player1 == choice_player2:
        print("Unentschieden, kein Gewinner!")
    else:
        if choice_player1 == "SCHERE":
            if choice_player2 == "STEIN":
                print("Computer gewinnt!")
                computer_score += 1
            elif choice_player2 == "PAPIER":
                print("Spieler gewinnt!")
                player_score += 1

        elif choice_player1 == "STEIN":
            if choice_player2 == "PAPIER":
                print("Computer gewinnt!")
                computer_score += 1
            elif choice_player2 == "SCHERE":
                print("Spieler gewinnt!")
                computer_score += 1

        elif choice_player1 == "PAPIER":
            if choice_player2 == "SCHERE":
                print("Computer gewinnt!")
                computer_score += 1
            elif choice_player2 == "STEIN":
                print("Spieler gewinnt!")
                computer_score += 1

        else:
            print("Uuups. Da ist etwas schief gelaufen!")
if computer_score < player_score:
    print(
        f"Spieler gewinnt mit einem Ergebnis von {player_score}:{computer_score}.")
elif computer_score > player_score:
    print(
        f"Computer gewinnt mit einem Ergebnis von {computer_score}:{player_score}.")
