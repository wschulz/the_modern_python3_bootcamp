# Tuples are an ordered collection or grouping of items.
# Tuples are IMMUTABLE!

numbers = (1, 2, 3, 4)

x = (1, 2, 3)
3 in x #True
# x[0] = "change me!" #TypeError: 'tuple' object does not support item assignment

alphabet = ('a', 'b', 'c', 'd')
print(alphabet)
print(type(alphabet))

# WHY USE A TUPLE

# Tuples are faster than lists
# It makes your code safer
# Valid keys in a dictionary
# Some methods return them to you - like .items() when working with dictionaries!

# Tuples are commonly used for NOT CHANGING data:
months = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
print(months[0])

# Tuples can be used as keys in dictionaries:
locations = {
    (35.6895, 39.6917): "Tokio Office",
    (40.7128, 74.0060): "New York Office",
    (37.7749, 122.4194): "San Francisco Office"
}
print(locations[(40.7128, 74.0060)])

# Some dictionary methods like .item() return tuples
cat = {"name": "biscuit", "age": 0.5, "favorite_toy": "my chapstick"}
print(cat.items()) # gives us a list with 3 tuples


# count
# Returns the number of times a value appears in a tuple

x = (1, 2, 3, 3, 3)
x.count(1) # 1
x.count(3) # 3


# index
# Returns the index at which a value is found in a tuple.

t = (1, 2, 3, 3, 3)
t.index(1) # 0
# t.index(5) # ValueError: tuple.index(x): x not in tuple
t.index(3) # 2 - only the first matching index is returned


# nested tuples
nums = (1, 2, 3, (4, 5), 6)
print(nums[0])
print(nums[3])
print(nums[3][1])