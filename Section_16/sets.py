# Sets are like formal mathematical sets.

# Sets do not have duplicate values

# Elemets in sets are not ordered.

# You cannot access items in a set by index.

# Sets can be useful if you need to keep track of a collection of elements,
# but don't care about ordering, keys or values and duplicates


# Sets cannot have duplicates
s = set({1, 2, 3, 4, 5, 5, 5}) # {1, 2, 3, 4, 5}

# Creating a new set
s = set({1, 4, 5})

# Creates a set with the same values as above
s = {1, 4, 5}

4 in s # True
8 in s # False

for element in s:
    print(element)


cities = ["Los Angeles", "Boulder", "Kyoto", "Florence", "Santiago", "Los Angeles", "Shanghai", "Boulder", "San Francisco", "Oslo", "Tokyo"]
print(list(set(cities)))
print(len(set(cities)))


#
# METHODS
#

# add
# Adds an element to a set. If the element is already in the set, the set doesn't change.

s = set([1, 2, 3])
s.add(4)
s # {1, 2, 3, 4}
s.add(2)
s # {1, 2, 3, 4}


# remove
# Remove a vlaue from a set - returns a KeyError if the value is not found

set1 = {1, 2, 3, 4, 5, 6}
set1.remove(3)
# set1.remove(9) # KeyError
print(set1)


# discard
# Avoids KeyErrors

set1.discard(9)
print(set1)


# copy
# Creates a copy of the set

s = set([1, 2, 3])
another_s = s.copy()
another_s # {1, 2, 3}
another_s == s # True
another_s is s # False


# clear
# Removes all the contents of the set

s = set([1, 2, 3])
s.clear()
s # set()


# Set Math

# Sets have quite a few other mathematical methods, including:
# intersection
# symmetric_difference
# union


math_students = {"Mathew", "Helen", "Prashant", "James", "Aparna"}
biology_students = {"Jane", "Mathew", "Charlotte", "Mesut", "Oliver", "James"}

# union
# Generates a set with all unique students
print(math_students | biology_students)

# intersection
# Generates a set with students who are in both courses
print(math_students & biology_students)


# SET COMPREHENSION

{x**2 for x in range(10)} # {0, 1, 64, 4, 36, 9, 16, 49, 81, 25}

{char.upper() for char in "hello"} # {'O', 'L', 'H', 'E'}

message = "sequoia"
len({char for char in message if char in "aeiou"}) == 5 # check if all vowels are in a message

# Same thing in a function
def are_all_vowels_in_string(message):
    return len({char for char in message if char in "aeiou"}) == 5