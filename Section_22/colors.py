from termcolor import colored

text = colored("Hi there!", color="yellow", on_color="on_cyan", attrs=["blink"])
print(text)