def full_name(first, last):
    return f"Your full name is {first} {last}"

print(full_name(first = "Hugo", last = "the Boss"))
print(full_name(last = "the Boss", first = "Hugo"))
