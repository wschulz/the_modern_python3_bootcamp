#def print_square_of_7():
#    print(7**2)
#
#print_square_of_7()


def square_of_7():
    return 7**2

result = square_of_7()
print(result)


# return

# Exits the function
# Outputs whatever value is placed after the return keyword
# Pops the function off of the call stack