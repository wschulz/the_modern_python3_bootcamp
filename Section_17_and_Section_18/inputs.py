def square(num):
    return num ** 2


def sing_happy_birthday(name):
    print("Happy Birthday To You")
    print("Happy Birthday To You")
    print(f"Happy Birthday Dear {name}")
    print("Happy Birthday To You")


def add(a, b):
    return a + b


def multiply(first, second):
    return first * second


# Default parameters
def exponent(num, power=2):
    return num ** power


def show_information(first_name="Colt", is_instructor=False):
    if first_name == "Colt" and is_instructor:
        return "Welcome back instructor Colt!"
    elif first_name == "Colt":
        return "I really thought you were an instructor..."
    return f"Hello {first_name}!"



def addition(a, b):
    return a + b

def subtraction(a, b):
    return a - b

def math(a, b, fn=add):
    return fn(a, b)

square(5)
sing_happy_birthday("Hansel")
print(multiply(12, 30))

print(exponent(2, 3))
print(exponent(3))
print(exponent(7))

print(show_information())
print(show_information(is_instructor=True))
print(show_information("Hugo"))

print(math(2,2))
print(math(2,2,subtraction))