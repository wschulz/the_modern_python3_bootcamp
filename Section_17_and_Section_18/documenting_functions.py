def say_hello():
    """A simple function that returns the string hello"""
    return "Hello!"

print(say_hello.__doc__) # A simple function that returns the string hello