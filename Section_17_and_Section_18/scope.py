# Vraiables created in functions are scoped in that function.
# Means: They are not available outside of the function.

instructor = "Colt" # This variable is defined in a global scope

def say_hello():
    return f"Hello {instructor}"

say_hello() # 'Hello Colt'


def say_hi():
    name = "Hugo" # This varaibale is scoped inside of the function only
    return f"Hi {name}"

say_hi() # 'Hi Hugo'
# print(name) # NameError


# global
# Reference Varaibles that were originally assigned on the global scope, to manipulate it

# total = 0

# def increment():
#     total += 1
#     return total
# 
# increment()

total = 0

def increment():
    global total
    total += 1
    return total

increment()


# nonlocal
# Lets us modify a parent's variable in a child (aka nested) function

def outer():
    count = 0
    def inner():
        nonlocal count
        count += 1
        return count
    return inner()